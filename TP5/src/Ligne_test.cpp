#include "Ligne.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLigne) { };

TEST(GroupLigne, Ligne_test1)  {
	Couleur yes=Couleur{1,0,2};
	Point p1=Point{3,2};
	Point p2=Point{4,1};
	Ligne testLig=Ligne(yes, p1, p2);

    CHECK_EQUAL(testLig.getP0()._x, 3);
    CHECK_EQUAL(testLig.getP0()._y, 2);
}

TEST(GroupLigne, Ligne_test2)  {
	Couleur yes=Couleur{1,0,2};
	Point p1=Point{3,2};
	Point p2=Point{4,1};
	Ligne testLig=Ligne(yes, p1, p2);

    CHECK_EQUAL(testLig.getP1()._x, 4);
    CHECK_EQUAL(testLig.getP1()._y, 1);
}

