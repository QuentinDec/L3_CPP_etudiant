#ifndef FIGUREGEOMETRIQUE_HPP
#define FIGUREGEOMETRIQUE_HPP

#include "Couleur.hpp"
#include <gtkmm.h>
#include <cairomm/context.h>
class FigureGeometrique
{
protected:
    Couleur _couleur;
public:
    FigureGeometrique(const Couleur &);
    const Couleur & getCouleur() const;
    virtual void afficher(const Cairo::RefPtr<Cairo::Context>&) const = 0;
    virtual ~FigureGeometrique();
};

#endif // FIGUREGEOMETRIQUE_HPP
