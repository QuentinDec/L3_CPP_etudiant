#include "ZoneDessin.hpp"
#include "Couleur.hpp"
#include "Point.hpp"
#include "PolygoneRegulier.hpp"
#include "Ligne.hpp"
#include <iostream>


ZoneDessin::ZoneDessin()
{
    signal_button_press_event().connect(sigc::mem_fun(*this,&ZoneDessin::gererClic));
    add_events(Gdk::BUTTON_PRESS_MASK);
    _figures.push_back(new PolygoneRegulier(Couleur{0.0,0.0,0.0},Point{200,200},10,3));
    _figures.push_back(new PolygoneRegulier(Couleur{0.5,0.5,0.5},Point{100,100},22,3));
    _figures.push_back(new PolygoneRegulier(Couleur{1,1,1},Point{1,1},1,3));
    _figures.push_back(new Ligne(Couleur {1.0,0.0,0.0}, Point{500,500}, Point{250,100}));
}

ZoneDessin::~ZoneDessin(){
    for(FigureGeometrique *f: _figures) delete f;
}


bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> &cr){
    for(FigureGeometrique *f: _figures) f->afficher(cr);
    return true;
}

bool ZoneDessin::gererClic(GdkEventButton *event) {
    if(event->button == 1){
        _figures.push_back(new PolygoneRegulier(Couleur{0.0,0.0,0.0},Point{event->x,event->y},10,3));
    }
    else if(event->button == 3){
        if(_figures.size()>0){
            delete _figures[_figures.size()-1];
            _figures.pop_back();
        }
    }
    auto window = get_window();
    window->invalidate(true);
    return true;
}
