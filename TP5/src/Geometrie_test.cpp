#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLigne) { };

TEST(GroupLigne, Ligne_test1)  {
    Ligne l(Couleur {0.0,0.5,1.0}, Point {0,1}, Point{2,3});

    CHECK_EQUAL(0,l.getP0()._x);
    CHECK_EQUAL(1,l.getP0()._y);
    CHECK_EQUAL(2,l.getP1()._x);
    CHECK_EQUAL(3,l.getP1()._y);
}

TEST_GROUP(GroupPoly) { };

TEST(GroupPoly, Poly_test1)  {
    PolygoneRegulier p(Couleur {0.0,1.0,0.0}, Point {100,200},50,5);

    CHECK_EQUAL(5,p.getNbPoints());
    CHECK_THROWS(std::string,p.getPoint(-1));
    CHECK_THROWS(std::string,p.getPoint(5));
}

