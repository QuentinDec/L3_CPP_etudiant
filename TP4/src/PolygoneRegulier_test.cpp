#include "PolygoneRegulier.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupPolygoneRegulier) { };

TEST(GroupPolygoneRegulier, PolygoneRegulier_test1)  {
	Couleur testCoul=Couleur{0,1,0};
	Point centre=Point{100,200};
	PolygoneRegulier testPoly=PolygoneRegulier(testCoul,centre,50,5);
	CHECK_EQUAL(testPoly.getNbPoints(),5);
}

TEST(GroupPolygoneRegulier, PolygoneRegulier_test2)  {
	Couleur testCoul=Couleur{0,1,0};
	Point centre=Point{100,200};
	PolygoneRegulier testPoly=PolygoneRegulier(testCoul,centre,50,5);
	CHECK_EQUAL(testPoly.getPoint(3)._x,59);
	CHECK_EQUAL(testPoly.getPoint(3)._y,170);
}

