#include <iostream>
#include "PolygoneRegulier.hpp"
#include <vector>

int main(){
	Couleur yes=Couleur{1,0,2};
	Point p1=Point{3,2};
	Point p2=Point{4,1};
	Ligne * l= new Ligne(yes, p1, p2);
	l->afficher();
	std::cout<<std::endl<<std::endl;
	
	Couleur coul=Couleur{0,1,0};
	Point centre=Point{100,200};
	PolygoneRegulier * TestPoly= new PolygoneRegulier(coul,centre,50,5);
	TestPoly->afficher();
	
	std::vector<FigureGeometrique*> figGeo{TestPoly,l};
	
	
	for(FigureGeometrique * ptrFigGeo : figGeo)
		ptrFigGeo->afficher();
		
	for(FigureGeometrique * ptrFigGeo : figGeo)
		delete ptrFigGeo;
	
	return 0;
}
