#include <iostream>
#include "PolygoneRegulier.hpp"
#include <math.h>

	PolygoneRegulier::PolygoneRegulier(const Couleur &couleur,const Point &centre,int rayon,int nbCotes):
		FigureGeometrique(couleur), _nbPoints(nbCotes), _points(new Point[nbCotes])
	{
		float degreTot=180;
		int cpt=0;
		for(float i=0; i<360;i+=360/nbCotes){
			Point P{(int)(centre._x+rayon*cos(i*M_PI/degreTot)),(int)(centre._y+rayon*sin(i*M_PI/degreTot))};
			*(_points+cpt++)=P;
		}
	};
	
	void PolygoneRegulier::afficher() const{
		std::cout<<"Polygone Regulier "<<getCouleur()._r<<"_"<<getCouleur()._g<<"_"<<getCouleur()._b;
		for(int i=0; i<getNbPoints();i++){
			std::cout<<" "<<(_points+i)->_x<<"_"<<(_points+i)->_y;
		}
		std::cout<<std::endl;
	}
	
	int PolygoneRegulier::getNbPoints() const{
		return _nbPoints;
	}
	
	const Point &PolygoneRegulier::getPoint(int indice) const{
		return *(_points+indice);
	}

	PolygoneRegulier::~PolygoneRegulier(){
		delete [] _points;
	}
