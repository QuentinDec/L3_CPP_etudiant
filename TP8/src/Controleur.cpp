#include "Controleur.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    _vues.push_back(std::make_unique<VueConsole>(*this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));

    for (auto & v : _vues)
      v->actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

std::string Controleur::getTexte() const{
    std::stringstream s;
    s << _inventaire;
    return s.str();
}

void Controleur::chargerInventaire(const std::string &fichier) {
    std::ifstream ifs(fichier);
    if(! ifs.is_open()) throw std::string("Erreur lors de l'ouverture du fichier.");
    Bouteille b;
    while(ifs >> b && !ifs.eof()){
        _inventaire._bouteilles.push_back(b);
    }
    for (auto & v : _vues)
      v->actualiser();
}


