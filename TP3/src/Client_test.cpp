#include "Client.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

	Client TestCli(42, "toto");

TEST(GroupClient, Client_test1)  {
    CHECK_EQUAL(TestCli.getId(), 42);
}

TEST(GroupClient, Client_test2)  {
    CHECK_EQUAL(TestCli.getNom(), "toto");
}


