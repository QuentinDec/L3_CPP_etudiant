#include <iostream>
#include "Magasin.hpp"

Magasin::Magasin():
	_idCourantClient(0), _idCourantProduit(0)
{}

int Magasin::nbClients() const{
	return _clients.size();
}

void Magasin::ajouterClient(std::string const & nom){
	_clients.push_back(Client(++_idCourantClient,nom));
}

void Magasin::afficherClients() const{
	for(Client i : _clients)
		i.afficherClient();
}

void Magasin::supprimerClient(int idClient){
	try{
		for(unsigned i=0; i<_clients.size()-1; i++){
			if(_clients[i].getId()==idClient){
				std::swap(_clients[i], _clients[_clients.size()-1]);
				_clients.pop_back();
				return;
			}
		}
		if(_clients[_clients.size()-1].getId()==idClient)
				_clients.pop_back();
		else
			throw std::string("Erreur: ce Client n'existe pas");
	}
	catch(std::string erreur){
		std::cerr<<erreur<<std::endl;
	}
}

int Magasin::nbProduits() const{
	return _produits.size();
}

void Magasin::ajouterProduit(std::string const & nom){
	_produits.push_back(Produit(++_idCourantProduit, nom));
}
void Magasin::afficherProduits() const{
		for(Produit i : _produits)
			i.afficherProduit();
}

void Magasin::supprimerProduit(int idProduit){
	try{
		for(unsigned i=0; i<_produits.size()-1; i++){
			if(_produits[i].getId()==idProduit){
				std::swap(_produits[i], _produits[_produits.size()-1]);
				_produits.pop_back();
				return;
			}
		}
		if(_produits[_produits.size()-1].getId()==idProduit)
			_produits.pop_back();
		else
			throw std::string("Erreur: ce produit n'existe pas");
	}
	catch(std::string erreur){
		std::cerr<<erreur<<std::endl;
	}
}

int Magasin::nbLocations() const{
	return _locations.size();
}
	
void Magasin::ajouterLocation(int idClient, int idProduit){
	try{
		for(unsigned i=0; i<_locations.size(); i++){
			if(_locations[i].id_Client==idClient && _locations[i].id_Produit==idProduit){
				throw std::string("Erreur: cette location existe déjà");
			}
		}
		_locations.push_back(Location{idClient,idProduit});
	}
	catch(std::string erreur){
		std::cerr<<erreur<<std::endl<<std::flush;
	}
}

void Magasin::afficherLocations() const{
	for(Location i : _locations)
		i.afficherLocation();
}

void Magasin::supprimerLocation(int idClient, int idProduit){
	try{
		for(unsigned i=0; i<_locations.size()-1; i++){
			if(_locations[i].id_Client==idClient && _locations[i].id_Produit==idProduit){
				std::swap(_locations[i], _locations[_locations.size()-1]);
				_locations.pop_back();
				return;
			}
		}
		if(_locations[_locations.size()-1].id_Client==idClient && _locations[_locations.size()-1].id_Produit==idProduit)
			_locations.pop_back();
		else
			throw std::string("Erreur: cette location n'existe pas");
	}
	catch(std::string erreur){
		std::cerr<<erreur<<std::endl;
	}
}

bool Magasin::trouverClientDansLocation (int idClient) const{
	
	for (unsigned i=0; i<_locations.size(); i++){
		if (_locations[i].id_Client==idClient)
			return true;
		}
	return false;
}

std::vector<int> Magasin::calculerClientsLibres() const{
	
	std::vector<int>Res;
	for (unsigned i=0; i<_clients.size();i++){
		if (!trouverClientDansLocation(_clients[i].getId()))
			Res.push_back(_clients[i].getId());
		}
	return Res;
}

bool Magasin::trouverProduitDansLocation (int idProduit) const{
	
	for (unsigned i=0; i<_locations.size(); i++){
		if (_locations[i].id_Produit==idProduit)
			return true;
		}
	return false;
}

std::vector<int> Magasin::calculerProduitsLibres() const{
	
	std::vector<int>Res;
	for (unsigned i=0; i<_produits.size();i++){
		if (!trouverProduitDansLocation(_produits[i].getId()))
			Res.push_back(_produits[i].getId());
		}
	return Res;
}


