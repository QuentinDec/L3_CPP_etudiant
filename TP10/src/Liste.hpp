
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>

// liste d'entiers avec itérateur
class Liste {
    private:
        struct Noeud {
            int _valeur;
            Noeud* _ptrNoeudSuivant;
        };
        Noeud* _ptrTete;
    public:
        class iterator {
            private:
                Noeud* _ptrNoeudCourant;
            public:
                iterator(Noeud* ptrNoeudCourant): _ptrNoeudCourant(ptrNoeudCourant){

                }

                const iterator & operator++() {
                    return iterator(*this->_ptrNoeudCourant->_ptrNoeudSuivant);
                }

                int& operator*() const {
                    return *this->_ptrNoeudCourant->_valeur;
                }

                bool operator!=(const iterator &) const {
                    return false;
                }

                friend Liste; 
        };

    public:
        void push_front(int) { 
        }

        int& front() const {
            static int nimpe;
            return nimpe;
        }

        void clear() {
        }

        bool empty() const {
            return true;
        }

        iterator begin() const {
            return iterator();
        }

        iterator end() const {
            return iterator();
        }

};

std::ostream& operator<<(std::ostream& os, const Liste&) {
    return os;
}

#endif

