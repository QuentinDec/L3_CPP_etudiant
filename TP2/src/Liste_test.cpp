#include "Liste.h"
#include <iostream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupeListe) { };

TEST(GroupeListe, Liste_test1)  {
	Liste * Test= new Liste();
    CHECK_EQUAL(Test->getElement(0), -1);
    delete Test;
}

TEST(GroupeListe, Liste_test2)  {
	Liste * Test= new Liste();
	Test->ajouterDevant(18);
    CHECK_EQUAL(Test->getElement(0), 18);
    delete Test;
}

TEST(GroupeListe, Liste_test3)  {
	Liste * Test= new Liste();
	Test->ajouterDevant(18);
	Test->ajouterDevant(24);
	Test->ajouterDevant(28);
	Test->ajouterDevant(36);
    CHECK_EQUAL(Test->getElement(2), 24);
    delete Test;
}

TEST(GroupeListe, Liste_test4)  {
	Liste * Test= new Liste();
	Test->ajouterDevant(18);
	Test->ajouterDevant(24);
	Test->ajouterDevant(28);
	Test->ajouterDevant(36);
    CHECK_EQUAL(Test->getTaille(), 4);
    delete Test;
}
