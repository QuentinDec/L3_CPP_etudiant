#include "vecteur3.h"
#include <CppUTest/CommandLineTestRunner.h>
TEST_GROUP(GroupVecteur3) { };

TEST(GroupVecteur3, test_Vect_Norm) { // premier test unitaire
    Vecteur3D vNorme{2.0,3.0,6.0};
    DOUBLES_EQUAL(7, vNorme.norme(),0.001);
}

TEST(GroupVecteur3, test_Vect_Sca) { // deuxieme test unitaire
    Vecteur3D vSScalaire{2.0,3.0,6.0};
    Vecteur3D vScalaire{1.0,2.0,5.0};
    DOUBLES_EQUAL(38, vSScalaire.produitScalaire(&vScalaire),0.001);
}

TEST(GroupVecteur3, test_Vect_Add) { // troisieme test unitaire
    Vecteur3D vSScalaire{2.0,3.0,6.0};
    Vecteur3D vScalaire{1.0,2.0,5.0};
    CHECK_EQUAL(3.0, vSScalaire.addition(&vScalaire).x);
    CHECK_EQUAL(5.0, vSScalaire.addition(&vScalaire).y);
    CHECK_EQUAL(11.0, vSScalaire.addition(&vScalaire).z);
}
