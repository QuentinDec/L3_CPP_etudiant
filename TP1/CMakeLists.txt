cmake_minimum_required( VERSION 3.0 )
project( MON_PROJET )
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" )

find_package( PkgConfig REQUIRED )

add_executable( main.out main.cpp Fibonacci.cpp vecteur3.cpp)

# programme de tests unitaires
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest )
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} )
add_executable( main_test.out main_test.cpp Fibonacci.cpp fibonacci_test.cpp vecteur3.cpp vecteur3_test.cpp )
target_link_libraries( main_test.out ${PKG_CPPUTEST_LIBRARIES} )
