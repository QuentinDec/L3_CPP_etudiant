#include "Livre.hpp"
#include <iostream>

Livre::Livre()
{

}

 Livre::Livre(const std::string &titre,const std::string &auteur, int annee):
 _titre(titre),_auteur(auteur),_annee(annee){
    std::string::size_type n;
    n = titre.find(';');
    if(n != std::string::npos) throw std::string("erreur : titre non valide (';' non autorisé)");
    n = titre.find('\n');
    if(n != std::string::npos) throw std::string("erreur : titre non valide ('\n' non autorisé)");
    n = auteur.find(';');
    if(n != std::string::npos) throw std::string("erreur : auteur non valide (';' non autorisé)");
    n = auteur.find('\n');
    if(n != std::string::npos) throw std::string("erreur : auteur non valide ('\n' non autorisé)");
 }

 const std::string & Livre::getTitre() const{
     return _titre;
 }

 const std::string & Livre::getAuteur() const{
     return _auteur;
 }

int Livre::getAnnee() const{
    return _annee;
}

bool Livre::operator<(const Livre &l) const {
    if(_auteur<l.getAuteur()) return true;
    else if(_auteur==l.getAuteur()) {
        if(_titre<l.getTitre()) return true;
        else return false;
    }
    return false;
}
bool operator==(const Livre &a, const Livre &b) {
    return (a.getAuteur()==b.getAuteur() and
            a.getTitre()==b.getTitre() and
            a.getAnnee()==b.getAnnee());
}

std::istream& operator>>(std::istream& is, Livre &l) {
    std::string titre;
    getline(is,titre,';');
    std::string auteur;
    getline(is,auteur,';');
    std::string annee;
    getline(is,annee);
    l = Livre(titre,auteur,std::stoi(annee));
    return is;
}

std::ostream& operator<<(std::ostream& os, Livre &l){
    os << l.getTitre() << ";" << l.getAuteur() << ";" << l.getAnnee();
}

