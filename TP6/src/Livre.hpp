#ifndef LIVRE_HPP
#define LIVRE_HPP
#include <string>
#include <istream>

class Livre
{
private:
    std::string _titre,_auteur;
    int _annee;

public:
    Livre();
    Livre(const std::string &_titre,const std::string &_auteur, int _annee);
    const std::string & getTitre() const;
    const std::string & getAuteur() const;
    int getAnnee() const;
    bool operator<(const Livre &l) const;
    friend std::istream& operator>>(std::istream& is, Livre &l);
    friend std::ostream& operator<<(std::ostream& os, Livre &l);

};
bool operator==(const Livre &a, const Livre &b);
#endif // LIVRE_HPP
